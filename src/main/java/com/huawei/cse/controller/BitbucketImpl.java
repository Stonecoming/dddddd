package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-10T12:17:56.471Z")

@RestSchema(schemaId = "bitbucket")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class BitbucketImpl {

    @Autowired
    private BitbucketDelegate userBitbucketDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userBitbucketDelegate.helloworld(name);
    }

}
